﻿using CESBilling.DualBilling.QueueAPI;
using CommonContracts.PreBillContracts;
using System;
using System.Collections.Generic;

namespace QueueItemProducer
{
    class Program
    {
        static string queueURL = "https://sqs.us-east-2.amazonaws.com/998557616265/PreBillQueue";

        static void Main(string[] args)
        {
            Console.WriteLine("Press any key to produce items");
            Console.ReadLine();
            //ProducePreBillQueue();
            ProducePreBillQueue_Generic();
            Console.WriteLine("Items produced for pre bill");
            Console.ReadLine();
        }


        private static void ProducePreBillQueue()
        {
           
           //PreBillProducer pre = new PreBillProducer();
           //List<PreBillQueueRequestContract> items= pre.GetPreBillReadyItems();
           //PreBillQueueControllerAPI api = new PreBillQueueControllerAPI();
           //api.PublishMessageBatchToQueue(items);

        }

        private static void ProducePreBillQueue_Generic()
        {

            PreBillProducer pre = new PreBillProducer();
            List<PreBillQueueRequestContract> items = pre.GetPreBillReadyItems();

            GenericQueueAPI api = new GenericQueueAPI();
            api.PublishMessageBatchToQueue<PreBillQueueRequestContract>(items, queueURL);
        }
    }
}
