﻿
using CommonContracts.PreBillContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QueueItemProducer
{
    public class PreBillProducer
    {
        public List<PreBillQueueRequestContract> GetPreBillReadyItems()
        {
            return GetPreBillItems();
        }

 

        private List<PreBillQueueRequestContract> GetPreBillItems()
        {
            List<PreBillQueueRequestContract> preBillReadyItems = new List<PreBillQueueRequestContract>();
            preBillReadyItems.Add(new PreBillQueueRequestContract { clientid = 70 });
            // Thread.Sleep(2000);
            Console.WriteLine("added");
            preBillReadyItems.Add(new PreBillQueueRequestContract { clientid = 80 });
            //Thread.Sleep(2000);
            Console.WriteLine("added");
            preBillReadyItems.Add(new PreBillQueueRequestContract { clientid = 90 });

            return preBillReadyItems;
        }
    }
}
