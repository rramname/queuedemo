﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;

namespace AWSEventPublisher
{
    public class SQSController
    {
        public AmazonSQSClient client;
        public SQSController()
        {
            AWSConfigs.LoggingConfig.LogTo = LoggingOptions.Log4Net;
            AWSConfigs.AWSProfilesLocation = @"C:\Rohit\RnD\AWSQueueDemo\credentials";
            AmazonSQSConfig amazonSQSConfig = new AmazonSQSConfig();
            //AWSConfigs.AWSProfileName = "Development";
            amazonSQSConfig.ServiceURL = "https://sqs.us-east-2.amazonaws.com/";
            //AWSConfigs.AWSProfilesLocation = @"C:\Rohit\RnD\AWSQueueDemo\credentials";
            client = new AmazonSQSClient(amazonSQSConfig);
        }
        public SQSController(string QueueURL)
        {
            AWSConfigs.LoggingConfig.LogTo = LoggingOptions.Log4Net;
            AWSConfigs.AWSProfilesLocation = @"C:\Rohit\RnD\AWSQueueDemo\credentials";
            AmazonSQSConfig amazonSQSConfig = new AmazonSQSConfig();
            //AWSConfigs.AWSProfileName = "Development";
            amazonSQSConfig.ServiceURL = "https://sqs.us-east-2.amazonaws.com/";

            //AWSConfigs.AWSProfilesLocation = @"C:\Rohit\RnD\AWSQueueDemo\credentials";
            client = new AmazonSQSClient(amazonSQSConfig);
        }

    }
}
