﻿using AWSEventPublisher.QueueControllers;
using CommonContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CESBilling.DualBilling.QueueAPI
{
    public class GenericQueueAPI
    {

        public void PublishMessageBatchToQueue<T>(List<T> batch,string queueUrl)
        {
            GenericQueueController controller = new GenericQueueController(queueUrl);
            controller.AddItemsBachToTheQueue<T>(batch);
        }
        
        public List<GenericQueueResponseContract<T>> GetMessagesFromQueue<T>(string queueUrl,int totalITems)
        {
            GenericQueueController controller = new GenericQueueController(queueUrl);
            return controller.GetItemsFromTheQueue<T>(totalITems);

        }
    }
}
