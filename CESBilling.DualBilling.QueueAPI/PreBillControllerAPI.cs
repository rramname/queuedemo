﻿using AWSEventPublisher;
using CommonContracts.PreBillContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CESBilling.DualBilling.QueueAPI
{
    public class PreBillQueueControllerAPI
    {
        public PreBillQueueControllerAPI()
        {

        }
        public void PublishSingleMessageToQueue()
        {
            PreBillQueueController controller = new PreBillQueueController();
            controller.AddSingleItemToTheQueue();
        }

        public void PublishMessageBatchToQueue(List<PreBillQueueRequestContract> batch)
        {
            PreBillQueueController controller = new PreBillQueueController();
            controller.AddItemsBachToTheQueue(batch);
        }

        public List<PreBillQueueResponseContract>  ReadMessagesFromQueue()
        {
            PreBillQueueController controller = new PreBillQueueController();
            return controller.GetItemFromTheQueue();
        }

    }
}
