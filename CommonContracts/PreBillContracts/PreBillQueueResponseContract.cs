﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonContracts.PreBillContracts
{
    public class PreBillQueueResponseContract
    {
        public string ReceiptHandle { get; set; }
        public PreBillQueueRequestContract request;
    }
}
