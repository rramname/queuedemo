﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonContracts.PreBillContracts
{
    public class PreBillQueueRequestContract
    {
        public int clientid { get; set; }
        public int acntUsageId { get; set; }
    }
}
