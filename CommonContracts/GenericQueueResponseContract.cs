﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonContracts
{
    public class GenericQueueResponseContract<T>
    {
        public string ReceiptHandle { get; set; }
        public T response;
    }
}
