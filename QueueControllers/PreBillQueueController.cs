﻿using Amazon.SQS;
using Amazon.SQS.Model;
using CommonContracts;
using CommonContracts.PreBillContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AWSEventPublisher
{
    public class PreBillQueueController : SQSController
    {
        private AmazonSQSClient _client;
        string queueURL = "https://sqs.us-east-2.amazonaws.com/998557616265/PreBillQueue";
        public PreBillQueueController()
        {
            _client = base.client;

        }

        public void AddSingleItemToTheQueue()
        {
            SendMessageRequest req = new SendMessageRequest();
            req.QueueUrl = queueURL;
            var result = _client.SendMessage(queueURL, "Test Pre Bill 2");
            Console.WriteLine("Message ID:" + result.MessageId);
        }

        public void AddItemsBachToTheQueue(List<PreBillQueueRequestContract> preBillQueueContract)
        {
            Console.WriteLine("Publising batch to the queue");
            Thread.Sleep(2000);
            List<SendMessageBatchRequestEntry> entries = new List<SendMessageBatchRequestEntry>();

            foreach (var item in preBillQueueContract)
            {
                SendMessageBatchRequestEntry req = new SendMessageBatchRequestEntry();
                req.MessageBody = Newtonsoft.Json.JsonConvert.SerializeObject(item);
                req.Id = item.clientid.ToString();
                entries.Add(req);
            }
            SendMessageBatchRequest batch = new SendMessageBatchRequest(queueURL, entries);
            var result = _client.SendMessageBatch(batch);

            if (result.Successful.Count == preBillQueueContract.Count)
            {
                Console.WriteLine("All items published successfully");
            }
        }

        public void DeleteSingleItemFromQueue()
        {
            DeleteMessageRequest req = new DeleteMessageRequest();
            req.QueueUrl = queueURL;
        }

        public void DeleteBatchFromQueue()
        {

        }

        public List<PreBillQueueResponseContract> GetItemFromTheQueue()
        {
            var req = new ReceiveMessageRequest();
            req.QueueUrl = queueURL;
            req.MaxNumberOfMessages = 2;
            var response = client.ReceiveMessage(req);
            List<PreBillQueueResponseContract> items = new List<PreBillQueueResponseContract>();
            foreach (var message in response.Messages)
            {
                PreBillQueueResponseContract item = new PreBillQueueResponseContract();
                item.ReceiptHandle = message.ReceiptHandle;
                item.request=Newtonsoft.Json.JsonConvert.DeserializeObject<PreBillQueueRequestContract>(message.Body);
                items.Add(item);
            }
            return items;
        }
    }
}
