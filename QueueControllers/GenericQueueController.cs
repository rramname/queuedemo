﻿using Amazon.SQS;
using Amazon.SQS.Model;
using CommonContracts;
using System;
using System.Collections.Generic;

namespace AWSEventPublisher.QueueControllers
{
    public class GenericQueueController : SQSController
    {
        private AmazonSQSClient _client;
        private string _queueURL;
        public GenericQueueController(string queueUrl)
        {
            _queueURL = queueUrl;
            
            _client = base.client;
        }

        public void AddSingleItemToTheQueue<T>(T queueItem)
        {
            SendMessageRequest req = new SendMessageRequest();
            req.QueueUrl = _queueURL;
            req.MessageBody= Newtonsoft.Json.JsonConvert.SerializeObject(queueItem);
            var result = _client.SendMessage(req);
            Console.WriteLine("Message ID:" + result.MessageId);
        }

        public void AddItemsBachToTheQueue<T>(List<T> qQueueContract)
        {
            Console.WriteLine("Publising batch to the queue");
            
            List<SendMessageBatchRequestEntry> entries = new List<SendMessageBatchRequestEntry>();
            string batchId = new Guid().ToString();
            int idCounter = 0;
            foreach (var item in qQueueContract)
            {
                SendMessageBatchRequestEntry req = new SendMessageBatchRequestEntry();
                req.MessageBody = Newtonsoft.Json.JsonConvert.SerializeObject(item);
                req.Id = idCounter.ToString();
                MessageAttributeValue val = new MessageAttributeValue();
                val.StringValue = batchId;
                //val.DataType = "string";
                //req.MessageAttributes.Add("BatchGUID", val);
                entries.Add(req);
                idCounter++;
            }
            SendMessageBatchRequest batch = new SendMessageBatchRequest(_queueURL, entries);
            var result = _client.SendMessageBatch(batch);

            if (result.Successful.Count == qQueueContract.Count)
            {
                Console.WriteLine("All items published successfully");
            }
            else
            {
                Console.WriteLine("{0} out of {1} items published to the queue",result.Successful.Count, qQueueContract.Count);
            }
        }

        public List<GenericQueueResponseContract<T>> GetItemsFromTheQueue<T>(int noOfMessage)
        {
            var req = new ReceiveMessageRequest();
            req.QueueUrl = _queueURL;
            req.MaxNumberOfMessages = noOfMessage;
            req.WaitTimeSeconds = 10;
            req.VisibilityTimeout = 15;
            var response = client.ReceiveMessage(req);
            List<GenericQueueResponseContract<T>> items = new List<GenericQueueResponseContract<T>>();
            foreach (var message in response.Messages)
            {
                GenericQueueResponseContract<T> item = new GenericQueueResponseContract<T>();
                item.ReceiptHandle = message.ReceiptHandle;
                item.response = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(message.Body);
                items.Add(item);
            }
            return items;
        }
    }
}
